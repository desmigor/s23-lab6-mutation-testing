import pytest
from bonus_system import calculateBonuses

# Define test data for different programs
standard_data = [
    (5000, 0.5),
    (9999, 0.5),
    (10000, 0.75),
    (45000, 0.75),
    (49999, 0.75),
    (50000, 1),
    (95000, 1),
    (99999, 1),
    (100000, 1.25),
    (999999, 1.25),
]

premium_data = [
    (5000, 0.1),
    (9999, 0.1),
    (10000, 0.15),
    (45000, 0.15),
    (49999, 0.15),
    (50000, 0.2),
    (95000, 0.2),
    (99999, 0.2),
    (100000, 0.25),
    (999999, 0.25),
]

diamond_data = [
    (5000, 0.2),
    (9999, 0.2),
    (10000, 0.3),
    (45000, 0.3),
    (49999, 0.3),
    (50000, 0.4),
    (95000, 0.4),
    (99999, 0.4),
    (100000, 0.5),
    (999999, 0.5),
]

invalid_data = [
    (None, 10000, 0),
    ("", 10000, 0),
    ("Invalid", 10000, 0),
]

@pytest.mark.parametrize("amount, expected", standard_data)
def test_standard_program(amount, expected):
    result = calculateBonuses("Standard", amount)
    assert round(result, 2) == expected

@pytest.mark.parametrize("amount, expected", premium_data)
def test_premium_program(amount, expected):
    result = calculateBonuses("Premium", amount)
    assert round(result, 2) == expected

@pytest.mark.parametrize("amount, expected", diamond_data)
def test_diamond_program(amount, expected):
    result = calculateBonuses("Diamond", amount)
    assert round(result, 2) == expected

@pytest.mark.parametrize("program, amount, expected", invalid_data)
def test_invalid_program(program, amount, expected):
    result = calculateBonuses(program, amount)
    assert round(result, 2) == expected
